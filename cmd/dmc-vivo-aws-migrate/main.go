package main

import (
	"log"
	"os"

	"bitbucket.org/luizsimples/dmc-vivo-aws-migrate/internal/algorithm"
	"bitbucket.org/luizsimples/dmc-vivo-aws-migrate/internal/env"
	"github.com/joho/godotenv"
	_ "github.com/lib/pq"
)

var hostAPI string
var tokenAPI string

func main() {
	log.SetOutput(os.Stdout)
	log.Printf("dmc-vivo-aws-migrate %s\r\n", hostAPI)

	if err := godotenv.Load(); err != nil {
		log.Fatal("Error loading .env file")
	}

	config := env.GetConfig(hostAPI, tokenAPI)
	algorithm.Execute(config)
}
