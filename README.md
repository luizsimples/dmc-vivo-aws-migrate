# dmc-vivo-aws-migrate

## Configuração

Variáveis de ambiente para execução

## Preparação da workspace

```shell
export GOPATH={path/to/your/workspace}
mkdir -p $(go env GOPATH)/src/bitbucket.org/luizsimples

cd $(go env GOPATH)/src/bitbucket.org/luizsimples
git clone git@bitbucket.org:luizsimples/dmc-vivo-aws-migrate.git
```

## Instalar dependências

```shell
cd $(go env GOPATH)/src/bitbucket.org/luizsimples/dmc-vivo-aws-migrate

go get github.com/onsi/ginkgo/ginkgo
go install github.com/onsi/ginkgo/ginkgo
go get -t ./...
```

## Executar os testes

```shell
$(go env GOPATH)/bin/ginkgo -v ./...
```

## Build local e rodar localmente

```shell
go build -o dmc-vivo-aws-migrate ./cmd/dmc-vivo-aws-migrate

./dmc-vivo-aws-migrate
```

## Build para produção

```shell
go build -ldflags="-s -w -X main.hostAPI=http://localhost:3000 -X main.tokenAPI=da39a3ee5e6b4b0d3255bfef95601890afd80709" -o dmc-vivo-aws-migrate ./cmd/dmc-vivo-aws-migrate/main.go
```
