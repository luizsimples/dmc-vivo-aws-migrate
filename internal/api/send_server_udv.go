package api

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"strings"
)

// SendServerUDV doc-me
func SendServerUDV(config *Config, info *AudioInfo, audioPath string, audioLength int) error {
	uri := fmt.Sprintf("%s%s", config.HostAPI, "/api/audios")
	parts := strings.Split(info.AudioName, ".")
	extn := parts[len(parts)-1]

	payload, _ := json.Marshal(map[string]interface{}{
		"date":             info.Date,
		"core":             info.Core,
		"degree":           info.Degree,
		"topics":           info.Topics,
		"speaker":          info.Speaker,
		"directing_mestre": info.DirectingMestre,
		"audio_extn":       extn,
		"audio_type":       "audio/mp3",
		"audio_name":       info.AudioName,
		"audio_secs":       info.AudioSecs,
		"audio_size":       strconv.FormatInt(int64(audioLength), 10),
		"audio_storage":    audioPath,
	})

	req, _ := http.NewRequest("POST", uri, bytes.NewBuffer(payload))
	req.Header.Add("Accept", "application/json")
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", config.TokenAPI))

	_, err := executeRequest(req)
	if err != nil {
		return err
	}

	return nil
}
