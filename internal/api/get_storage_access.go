package api

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strconv"
)

// GetStorageAccess doc-me
func GetStorageAccess(config *Config, audioLength int, audioName string) (*SignedPost, error) {
	uri := fmt.Sprintf("%s%s", config.HostAPI, "/api/audios/signature")
	req, _ := http.NewRequest("GET", uri, nil)
	req.Header.Add("Accept", "application/json")
	req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", config.TokenAPI))

	q := req.URL.Query()
	q.Set("fileLength", strconv.FormatInt(int64(audioLength), 10))
	q.Set("fileName", audioName)
	req.URL.RawQuery = q.Encode()

	response, err := executeRequest(req)
	if err != nil {
		return nil, err
	}

	signedPost := SignedPost{}
	err = json.Unmarshal(response, &signedPost)
	if err != nil {
		return nil, errors.New(string(response) + " - " + audioName)
	}

	return &signedPost, nil
}
