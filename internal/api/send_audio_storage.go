package api

import (
	"bytes"
	"encoding/xml"
	"errors"
	"mime/multipart"
	"net/http"
	"strconv"
)

// SendAudioStorage doc-me
func SendAudioStorage(audioBin *[]byte, audioLength int, signedPost *SignedPost) (string, error) {
	body := new(bytes.Buffer)
	writer := multipart.NewWriter(body)

	for field, val := range signedPost.Fields {
		writer.WriteField(field, val)
	}

	writer.WriteField("Content-Type", "audio/mp3")
	writer.WriteField("Content-Length", strconv.FormatInt(int64(audioLength), 10))

	part, _ := writer.CreateFormFile("file", signedPost.Fields["key"])
	part.Write(*audioBin)
	audioBin = nil

	contentType := writer.FormDataContentType()

	if err := writer.Close(); err != nil {
		return "", err
	}

	req, _ := http.NewRequest("POST", signedPost.URL, body)
	req.Header.Add("Content-Type", contentType)

	response, err := executeRequest(req)
	if err != nil {
		return "", err
	}

	awsResult := AwsResult{}
	xml.Unmarshal(response, &awsResult)

	if awsResult.Message != nil {
		return "", errors.New(*awsResult.Message)
	}

	return *awsResult.File, nil
}
