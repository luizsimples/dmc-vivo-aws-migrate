package api

import (
	"io/ioutil"
	"net/http"
)

// executeRequest doc-me
func executeRequest(req *http.Request) ([]byte, error) {
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}

	body, err := ioutil.ReadAll(res.Body)
	res.Body.Close()

	if err != nil {
		return nil, err
	}

	return body, nil
}
