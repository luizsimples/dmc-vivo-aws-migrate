package api

import (
	"bitbucket.org/luizsimples/dmc-vivo-aws-migrate/internal/database"
	"bitbucket.org/luizsimples/dmc-vivo-aws-migrate/internal/env"
)

// SignedPost doc-me
type SignedPost struct {
	URL    string            `json:"url"`
	Fields map[string]string `json:"fields"`
}

// AwsResult doc-me
type AwsResult struct {
	File    *string `xml:"Key"`
	Message *string `xml:"Message"`
}

// AudioInfo Audio model
type AudioInfo = database.AudioInfo

// Config doc-me
type Config = env.Config
