package database

import (
	"database/sql"
	"fmt"

	"bitbucket.org/luizsimples/dmc-vivo-aws-migrate/internal/env"
)

// Connect Connect with database
func Connect(config *env.DB) (*sql.DB, error) {
	dbHost := fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=disable", config.UserName, config.Password, config.Hostname, config.Port, config.Database)
	db, err := sql.Open("postgres", dbHost)

	if err != nil {
		return nil, err
	}

	if errPing := db.Ping(); errPing != nil {
		return nil, errPing
	}

	return db, nil
}
