package database

// AudioInfo Audio model
type AudioInfo struct {
	ID int

	Date    string
	Core    string
	Degree  string
	Topics  string
	Speaker string

	DirectingMestre string

	AudioIV        string
	AudioName      string
	AudioSecs      string
	AudioEncrypted string
}

// OnReadAudio Function-callback when to read a audio from database
type OnReadAudio = func(*AudioInfo)
