package database

import (
	"database/sql"
	"log"
	"strings"
)

func empty(val string) bool {
	return strings.Trim(val, " ") == ""
}

// LoadCollections Load database audios
func LoadCollections(db *sql.DB, onReadAudio OnReadAudio) {
	rows, err := db.Query(`
		SELECT a.id,
			   CAST(s.session_date AS VARCHAR)  AS date,

			   COALESCE(s.nucleo,  '') AS core,
			   COALESCE(s.degree,  '') AS degree,
			   COALESCE(f.topics,  '') AS topics,
			   COALESCE(f.speaker, '') AS speaker,

			   COALESCE(s.directing_mestre,        '') AS directing_mestre,
			   COALESCE(a.encrypted_audio_data_iv, '') AS encrypted_iv,

			   COALESCE(f.name,                 '') AS audio_name,
			   CAST(f.audio_time AS VARCHAR)        AS audio_secs,
			   COALESCE(a.encrypted_audio_data, '') AS encrypted_audio

		FROM udv_sessions     AS s
		JOIN session_sections AS f ON (s.id = f.udv_session_id)
		JOIN audios           AS a ON (f.id = a.session_section_id)
		ORDER BY s.session_date, f.audio_id
	`)

	if err != nil {
		panic(err)
	}

	for rows.Next() {
		info := &AudioInfo{}

		err := rows.Scan(
			&info.ID,

			&info.Date,
			&info.Core,
			&info.Degree,
			&info.Topics,
			&info.Speaker,

			&info.DirectingMestre,

			&info.AudioIV,
			&info.AudioName,
			&info.AudioSecs,
			&info.AudioEncrypted,
		)

		if err != nil {
			log.Printf("Error on record read. %v", err)
			continue
		}

		if empty(info.AudioEncrypted) {
			log.Printf("Error on record read. ID -> %v: empty audio data", info.ID)
			continue
		}

		onReadAudio(info)
	}

	rows.Close()
}
