package env

// DB Database Environment
type DB struct {
	Port     string
	Hostname string
	Database string
	UserName string
	Password string
}

// Config Environment configuration
type Config struct {
	DB *DB

	HostAPI    string
	TokenAPI   string
	DecryptKey string
}
