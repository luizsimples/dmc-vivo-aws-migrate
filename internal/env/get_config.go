package env

import "os"

// GetConfig Get environment config
func GetConfig(hostAPI, tokenAPI string) *Config {
	port := os.Getenv("DB_PORT")
	if port == "" {
		port = "5432"
	}

	db := &DB{
		Port:     port,
		Hostname: os.Getenv("DB_HOST"),
		Database: os.Getenv("DB_DATABASE"),
		UserName: os.Getenv("DB_USERNAME"),
		Password: os.Getenv("DB_PASSWORD"),
	}

	return &Config{
		DB: db,

		HostAPI:    hostAPI,
		TokenAPI:   tokenAPI,
		DecryptKey: os.Getenv("SDB_AUDIO_HELPER"),
	}
}
