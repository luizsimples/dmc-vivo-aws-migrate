package algorithm

import (
	"fmt"
	"log"

	"bitbucket.org/luizsimples/dmc-vivo-aws-migrate/internal/api"
	"bitbucket.org/luizsimples/dmc-vivo-aws-migrate/internal/database"
	"bitbucket.org/luizsimples/dmc-vivo-aws-migrate/internal/env"
)

// AudioInfo Audio model
type AudioInfo = database.AudioInfo

// Execute doc-me
func Execute(config *env.Config) {
	db, err := database.Connect(config.DB)
	check(err)

	database.LoadCollections(db, func(info *AudioInfo) {
		infoBin := decrypter(config.DecryptKey, info.AudioIV, info.AudioEncrypted)
		info.AudioEncrypted = ""
		infoLength := len(*infoBin)

		storageAccess, err := api.GetStorageAccess(config, infoLength, info.AudioName)
		if logErr(err) {
			return
		}

		infoPath, err := api.SendAudioStorage(infoBin, infoLength, storageAccess)
		if logErr(err) {
			return
		}

		err = api.SendServerUDV(config, info, infoPath, infoLength)
		if logErr(err) {
			return
		}

		log.Println(fmt.Sprintf("Success #%v - %s", info.ID, info.AudioName))
	})
}
