package algorithm

import (
	"crypto/aes"
	"crypto/cipher"
	"encoding/base64"
	"fmt"
)

func check(err error) {
	if err != nil {
		panic(err)
	}
}

func logErr(err error) bool {
	if err != nil {
		fmt.Println(err)
		return true
	}

	return false
}

func decrypter(key, iv, encrypted64 string) *[]byte {
	nonce, err := base64.StdEncoding.DecodeString(iv)
	check(err)

	encryptedBin, err := base64.StdEncoding.DecodeString(encrypted64)
	check(err)

	keyAES256 := []byte(key[:32])
	aes256, err := aes.NewCipher(keyAES256)
	check(err)

	aes256gcm, err := cipher.NewGCM(aes256)
	check(err)

	decryptedBin, err := aes256gcm.Open(nil, nonce, encryptedBin, nil)
	check(err)

	return &decryptedBin
}
